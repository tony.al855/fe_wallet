import React from "react";
import './App.css';
import LoginPages from './pages/auth/LoginPage'
import RegisterPage from './pages/auth/RegisterPage'
import {BrowserRouter, Routes, Route} from "react-router-dom";
import HomePage from "./pages/HomePage";
import TopupPage from "./pages/wallet/TopupPage"
import TransferPage from "./pages/wallet/TransferPage"
import Header from './component/Header'

function App() {
  const tokenString = sessionStorage.getItem('token')

  return (
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<LoginPages/>}/>
          <Route path="/register" element={<RegisterPage/>}/>
          <Route path="/home" element={<><Header/><HomePage/></>}/>
          <Route path="/topup" element={<><Header/><TopupPage/></>}/>
          <Route path="/transfer" element={<><Header/><TransferPage/></>}/>
        </Routes>
    </BrowserRouter>
  );
}

export default App;


import Container from 'react-bootstrap/Container';
import axios from "axios";
import { useNavigate, useEffect  } from 'react-router-dom';
import React, { useState } from "react";
import { Card, Col, Row, Table } from 'react-bootstrap';
import { Bar } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
} from "chart.js";
ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);


function HomePage() {
  const [balance, setBalance] = useState("");
  const [data, setData] = useState([]);
  const tokenString = sessionStorage.getItem('token')
  const nav = useNavigate();
  const [label, setLabel] = useState([])

  React.useEffect(() => {
    getBalance();
    getTopUser();

  }, []);

  const getBalance = async() => {
      await axios.get(process.env.REACT_APP_API_URL + '/balance',
      {
          headers: {
              "Authorization": tokenString,
            },
      }
      ).then((resp) => {
          setBalance(String(resp.data.data))
      })
      .catch((error) => {
          console.log(error);
          nav('/');
      })
    }

    const getTopUser = async() => {
      await axios.get(process.env.REACT_APP_API_URL + '/top_users',
      {
          headers: {
              "Authorization": tokenString,
            },
      }
      ).then((resp) => {
          console.log(resp.data.topTf);
          setData(resp.data.topTf);
      })
      .catch((error) => {
        console.log(error);
        nav('/');
      })
    }


  //statistic
  const options = {
    responsive: false,
    plugins: {
      legend: {
        position: "top"
      },
    }
  };
  
  const labels = ["January", "February", "March", "April", "May", "June", "July"];
  
  const dataStatistic = {
    labels,
    datasets: [
      {
        label: "DUMMY Count Tranfer Per Day",
        data: [2,4,5,6,1,4,5],
        backgroundColor: "rgba(53, 162, 235, 0.5)"
      }
    ]
  };


  return (
   <Container>
    <Row className='p-2'>
      <Col>
        <Card className='border-0'>
          <Card.Body>My Balance: {balance}</Card.Body>
        </Card>
      </Col>
    </Row>
    <Row className='p-3'>
      <Col>
        <Card className='border-0'>
          <Bar options={options} data={dataStatistic} />
        </Card>
      </Col>
    </Row>
    <Row>
      <h1>Top 10 Transactions</h1>
      <Col>
        <Card>
        <Table striped bordered hover size='sm'>
      <thead>
        <tr>
          <th>No</th>
          <th>Username</th>
          <th>Total Amount</th>
        </tr>
      </thead>
      <tbody>
      {data.map((val, index) =>
        <tr key={index}>
          <th>{index+1}</th>
          <th>{val.username}</th>
          <th>{val.transacted_value}</th>
        </tr>
      )}
      </tbody>
    </Table>
    
        </Card>
      </Col>
    </Row>
   </Container>
  );
}

export default HomePage;
import { Container, Row, Col, Card, Button, Form } from "react-bootstrap";
import React, { useState } from "react";
import axios from "axios";
import './Login.css';
import { useNavigate } from 'react-router-dom';
import { Link } from "react-router-dom";

export default function LoginPages(){
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [errorMessages, setErrorMessages] = useState("");
    const nav = useNavigate();

    const handleLogin = async(e) => {

        e.preventDefault();
        
        await axios.post(process.env.REACT_APP_API_URL + '/login',{
            username:username,
            password:password
        },
        // {
        //     headers: {
        //         "Access-Control-Allow-Origin": "*",
        //         "Content-Type": "application/json",
        //       },
        // }
        ).then((resp) => {
            sessionStorage.setItem("token", resp.data.token)
            nav('/home');
        })
        .catch((error) => {

            console.log(error);
            if (error.response.data.statusCode == 401){   
                setErrorMessages("Unauthorized");
            }else{
                setErrorMessages("Something Wrong");
            }
        })
    }

    const renderErrorMessage = () =>
    (
      <div className="error">{errorMessages}</div>
    );


    return (
        <Container className="mt-3">
        <Row>
            <Col md="{12}">
                <Card className="border-0 rounded shadow-sm cardLogin" style={{textAlign:"center"}}>
                    <Card.Body className="p-4">
                    <h1 >Login</h1>
                    <Form onSubmit={handleLogin}>
                    <Form.Group className="mb-3" controlId="formBasicUsername">
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="text" placeholder="Enter Username" value={username} onChange={(e) => setUsername(e.target.value)} required/>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Login
                    </Button>
                    {renderErrorMessage()}
                    </Form>
                    <Link to="register">Register</Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
    )
}

import React, { useState } from "react";
import axios from "axios";
import { Container, Row, Col, Card, Button, Form } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';


export default function Topup(){
    const [amount, setAmount] = useState("");
    const [errorMessages, setErrorMessages] = useState("");
    const nav = useNavigate();
    const tokenString = sessionStorage.getItem('token')
    

    const reqTopup = async(e) => {

        e.preventDefault();
        
        await axios.post(process.env.REACT_APP_API_URL + '/topup',{
            amount:Number(amount),
        },
        {
            headers: {
                "Authorization": tokenString,
              },
        }
        ).then((resp) => {
            nav('/home');
        })
        .catch((error) => {

            console.log(error);
            if (error.response.data.statusCode == 401){   
                setErrorMessages("Unauthorized");
                nav('/');
            }else if(error.response.data.statusCode == 400){
                setErrorMessages("Invalid topup amount");
            }else{
                setErrorMessages("Something Wrong");
            }
        })
    }

    const renderErrorMessage = () =>
    (
      <div className="error" style={{color:"red"}}>{errorMessages}</div>
    );


    return (
        <Container className="mt-3">
        <Row>
            <Col md="{12}">
                <Card className="border-0 rounded shadow-sm cardLogin" style={{textAlign:"center"}}>
                    <Card.Body className="p-4">
                    <h1 >Topup</h1>
                    <Form onSubmit={reqTopup}>
                    <Form.Group className="mb-3" controlId="formBasicUsername">
                        <Form.Label>Amount</Form.Label>
                        <Form.Control type="number" placeholder="Enter Amount" value={amount} onChange={(e) => setAmount(e.target.value)} required/>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Topup
                    </Button>
                    {renderErrorMessage()}
                    </Form>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
    )
}